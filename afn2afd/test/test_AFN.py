from unittest import TestCase
import pandas as pd
from afn2afd import AFN

class TestAFN(TestCase):
    df = pd.read_csv('../data/automatas.csv', sep=';')
    print(df)
    afn1 = AFN(df)
    estados = df['estados'].values

    def test_get_estados(self):
        # Arrange
        afn = self.afn1

        # Act
        result = afn.get_estados()

        # Assert
        TestCase.assertEqual(result, self.estados)

    def test_get_alfabeto(self):
        self.fail()

    def test_get_transiciones(self):
        self.fail()

    def test_get_estados_iniciales(self):
        self.fail()

    def test_get_estados_finales(self):
        self.fail()
