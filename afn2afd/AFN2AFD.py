#from afn2afd import AFN


class AFN2AFD:
    def __init__(self, afn, *args, **kwargs):
        self.__AFN = afn if self.__AFN_is_correct(afn) else None
        self.__AFD = None
        self.__generate_AFD()

    def get_AFN(self):
        return self.__AFN

    def get_AFD(self):
        #return self.__AFD
        return 'We are working on it! 💪'

    def __AFN_is_correct(self, afn):
        if afn is None:
            raise Exception("No se ha ingresado un AFN para analizar")
        elif afn.__class__.__name__ is not "AFN":
            raise Exception("El objeto ingresado como AFN no corresponde a una instancia de AFN")
        return True

    def __generate_AFD(self):

        estados = self.__AFN.get_estados()
        estados_iniciales = self.__AFN.get_estados_iniciales()
        estados_finales = self.__AFN.get_estados_finales()
        transiciones = self.__AFN.get_transiciones()

    def __delta(self, estado, caracter):
        if estado is dict:
            return self.__delta()
