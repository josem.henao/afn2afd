import numpy as np
import pandas as pd
from AFN2AFD import AFN2AFD

class AFN:
    """
    class AFN
    """
    def __init__(self, df):
        self.__df = df
        self.__estados = self.__extraer_estados()
        self.__alfabeto = self.__extraer_alfabeto()
        self.__transiciones = self.__extraer_transiciones()
        self.__estados_iniciales = self.__extraer_estados_iniciales()
        self.__estados_finales = self.__extraer_estados_finales()
        print(df)

    def __extraer_estados(self):
        try:
            return self.__df['estados'].values
        except:
            raise Exception(
                "No se ha ingresado un columna llamada 'estados' con los estados del AFN")

    def __extraer_alfabeto(self):
        try:
            return self.__df.columns[1:]
        except:
            raise Exception(
                "Hay un problema a la hora de extraer el alfabeto del dataset")

    def __extraer_transiciones(self):
        print("Extrayendo transiciones")
        try:
            return self.__df[self.__df.columns[1:]].values
        except:
            raise Exception(
                "No se han podido extraer las transiciones del dataset")

    def __extraer_estados_iniciales(self):
        try:
            return np.array([e for e in self.__estados if ('>' in e)])
        except:
            raise TypeError(
                "Se ha ingresado un estado sin el formato str esperado")

    def __extraer_estados_finales(self):
        try:
            return np.array([e for e in self.__estados if ('*' in e)])
        except:
            raise TypeError(
                "Se ha ingresado un estado sin el formato str esperado")

    def get_estados(self):
        return self.__estados

    def get_alfabeto(self):
        return self.__alfabeto

    def get_transiciones(self):
        return self.__transiciones

    def get_estados_iniciales(self):
        return self.__estados_iniciales

    def get_estados_finales(self):
        return self.__estados_finales


    def get_as_AFD(self):
        return AFN2AFD(self).get_AFD()

# df = pd.read_csv('data/automatas.csv', sep=';')
# tt = AFN(estados=np.array(['>q0', 'q1*', 'q2', 'q3', 'q4*']),
#          alfabeto=np.array(['0', '1']),
#          transiciones=df
#          )
#
# print('estados_iniciales:', tt.get_estados_iniciales())
# print('estados_finales:', tt.get_estados_finales())
# print('estados:', tt.get_estados())
# print('alfabeto:', tt.get_alfabeto())
# print('transiciones:\n', tt.get_transiciones())
